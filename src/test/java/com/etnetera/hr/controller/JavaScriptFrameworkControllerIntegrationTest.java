package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.service.JavascriptFrameworkService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class JavaScriptFrameworkControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	JavascriptFrameworkService service;

	static JavaScriptFramework framework1;
	static JavaScriptFramework framework2;


	@BeforeAll
	static void setUp() {

		framework1 = createBasicFramework("framework1", 1L);
		framework1.getVersions().add(createBasicVersion("version1", framework1));
		framework1.getVersions().add(createBasicVersion("version2", framework1));

		framework2 = createBasicFramework("framework2", 2L);
		framework2.getVersions().add(createBasicVersion("version1", framework1));
		framework2.getVersions().add(createBasicVersion("version2", framework1));
	}

	@Test
	void frameworks() throws Exception {
		Mockito.doReturn(Arrays.asList(framework1, framework2)).when(service).getFrameworks();
		mockMvc.perform(MockMvcRequestBuilders.get("/frameworks"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].name", is(framework1.getName())))
				.andExpect(jsonPath("$[0].id", is(framework1.getId().intValue())))
				.andExpect(jsonPath("$[0].versions[0].name", is(framework1.getVersions().get(0).getName())))
				.andExpect(jsonPath("$[1].name", is(framework2.getName())))
				.andExpect(jsonPath("$[1].id", is(framework2.getId().intValue())))
				.andExpect(jsonPath("$[1].versions[1].name", is(framework2.getVersions().get(1).getName())));

	}

	@Test
	void findFrameworkById() throws Exception {
		Mockito.doReturn(Optional.of(framework1)).when(service).getFrameworkById(1L);
		mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("name", is(framework1.getName())))
				.andExpect(jsonPath("id", is(framework1.getId().intValue())))
				.andExpect(jsonPath("versions[0].name", is(framework1.getVersions().get(0).getName())));

		mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/2"))
				.andExpect(status().isNotFound());
	}

	@Test
	void searchFrameworks() throws Exception {
		Mockito.doReturn(Arrays.asList(framework1, framework2)).when(service).getFrameworkByNameInfix("rame");
		mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/search")
				.requestAttr("nameInfix", "rame"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].name", is(framework1.getName())))
				.andExpect(jsonPath("$[0].id", is(framework1.getId().intValue())))
				.andExpect(jsonPath("$[0].versions[0].name", is(framework1.getVersions().get(0).getName())))
				.andExpect(jsonPath("$[1].name", is(framework2.getName())))
				.andExpect(jsonPath("$[1].id", is(framework2.getId().intValue())))
				.andExpect(jsonPath("$[1].versions[1].name", is(framework2.getVersions().get(1).getName())));

		mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/search")
				.requestAttr("nameInfix", "test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	void createFramework() throws Exception {
		Mockito.doReturn(Optional.of(framework2)).when(service).getFrameworkByName(framework2.getName());
		mockMvc.perform(MockMvcRequestBuilders.post("/frameworks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(framework1)))
				.andExpect(status().isOk());


		// test call with existing framework
		mockMvc.perform(MockMvcRequestBuilders.post("/frameworks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(framework2)))
				.andExpect(status().isOk());

		verify(service, times(1)).saveFramework(any());
	}

	@Test
	void updateFramework() throws Exception {
		Mockito.doReturn(Optional.of(framework1)).when(service).getFrameworkById(framework1.getId());
		mockMvc.perform(MockMvcRequestBuilders.put("/frameworks/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(framework1)))
				.andExpect(status().isOk());

		// test call with nonexistent framework
		mockMvc.perform(MockMvcRequestBuilders.put("/frameworks/2")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(framework2)))
				.andExpect(status().isNotFound());

		verify(service, times(1)).saveFramework(any());
	}

	@Test
	void deleteFramework() throws Exception {
		Mockito.doReturn(Optional.of(framework1)).when(service).getFrameworkById(framework1.getId());
		mockMvc.perform(MockMvcRequestBuilders.delete("/frameworks/1"))
				.andExpect(status().isOk());

		// test call with nonexistent framework
		mockMvc.perform(MockMvcRequestBuilders.delete("/frameworks/2"))
				.andExpect(status().isNotFound());

		verify(service, times(1)).deleteFramework(any());
	}

	@Test
	void createVersion() throws Exception {
		Mockito.doReturn(Optional.of(framework1)).when(service).getFrameworkById(framework1.getId());
		mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/1/versions")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(createBasicVersion("newVersion", null))))
				.andExpect(status().isOk());

		// test call with nonexistent framework
		mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/3/versions")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsBytes(framework1.getVersions().get(0))))
				.andExpect(status().isNotFound());

		verify(service, times(1)).saveVersion(any());
	}

	static JavaScriptFramework createBasicFramework(String name, Long id) {
		return JavaScriptFramework.builder()
				.id(id)
				.name(name)
				.hypeLevel(1)
				.build();
	}

	static JavaScriptFrameworkVersion createBasicVersion(String name, JavaScriptFramework framework) {
		return JavaScriptFrameworkVersion.builder()
				.framework(framework)
				.name(name)
				.build();
	}
}