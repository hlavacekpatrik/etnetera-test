package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class JavascriptFrameworkServiceImpl implements JavascriptFrameworkService {

	private final JavaScriptFrameworkRepository frameworkRepository;

	private final JavaScriptFrameworkVersionRepository versionRepository;

	public JavascriptFrameworkServiceImpl(JavaScriptFrameworkRepository frameworkRepository,
										  JavaScriptFrameworkVersionRepository versionRepository) {
		this.frameworkRepository = frameworkRepository;
		this.versionRepository = versionRepository;
	}

	@Override
	@Transactional
	public Optional<JavaScriptFramework> getFrameworkByName(String name) {
		return frameworkRepository.findByName(name);
	}

	@Override
	@Transactional
	public List<JavaScriptFramework> getFrameworkByNameInfix(String nameInfix) {
		return frameworkRepository.findByNameLike(nameInfix);
	}

	@Override
	@Transactional
	public Iterable<JavaScriptFramework> getFrameworks() {
		return frameworkRepository.findAll();
	}

	@Override
	@Transactional
	public Optional<JavaScriptFramework> getFrameworkById(Long id) {
		return frameworkRepository.findById(id);
	}

	@Override
	@Transactional
	public void saveFramework(JavaScriptFramework framework) {
		frameworkRepository.save(framework);
	}

	@Override
	@Transactional
	public void deleteFramework(JavaScriptFramework framework) {
		frameworkRepository.delete(framework);
	}

	@Override
	@Transactional
	public void saveVersion(JavaScriptFrameworkVersion version) {
		versionRepository.save(version);
	}
}
