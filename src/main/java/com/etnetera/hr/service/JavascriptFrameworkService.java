package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;

import java.util.List;
import java.util.Optional;

/**
 * Service for managing javascript frameworks and its versions.
 */
public interface JavascriptFrameworkService {

	/**
	 * Return all frameworks.
	 */
	Iterable<JavaScriptFramework> getFrameworks();

	/**
	 * Returns javascript framework by its id.
	 */
	Optional<JavaScriptFramework> getFrameworkById(Long id);

	/**
	 * Returns javascript framework with provided name.
	 */
	Optional<JavaScriptFramework> getFrameworkByName(String name);

	/**
	 * Returns javascript frameworks with name line provided infix.
	 */
	List<JavaScriptFramework> getFrameworkByNameInfix(String nameInfix);

	void saveFramework(JavaScriptFramework framework);

	/**
	 * Delete javascript framework and its versions.
	 */
	void deleteFramework(JavaScriptFramework framework);

	/**
	 * Save new version of javascript framework.
	 */
	void saveVersion(JavaScriptFrameworkVersion version);
}
