package com.etnetera.hr.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Data entity for version of {@link JavaScriptFramework}
 *
 * @author Patrik Hlaváček
 */
@Entity
@Data
@ToString(exclude = "framework")
@Builder
@NoArgsConstructor // constructor for database
@AllArgsConstructor // constructor for builder
public class JavaScriptFrameworkVersion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private JavaScriptFramework framework;

	@Column(nullable = false, length = 30)
	private String name;

	@Column(nullable = false)
	private LocalDateTime deprecationDate;

}
