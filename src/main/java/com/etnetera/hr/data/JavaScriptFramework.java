package com.etnetera.hr.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple data entity describing basic properties of every javaScript framework.
 *
 * @author Etnetera
 */
@Entity
@Data
@Builder
@NoArgsConstructor // constructor for database
@AllArgsConstructor // constructor for builder
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, unique = true, length = 30)
	private String name;

	@Builder.Default
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "framework")
	private List<JavaScriptFrameworkVersion> versions = new ArrayList<>();

	@Column(nullable = false)
	private Integer hypeLevel;

}

