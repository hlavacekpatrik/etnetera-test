package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.JavascriptFrameworkForm;
import com.etnetera.hr.dto.JavascriptFrameworkVersionForm;
import com.etnetera.hr.service.JavascriptFrameworkService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
public class JavaScriptFrameworkController {

	private final JavascriptFrameworkService service;

	public JavaScriptFrameworkController(JavascriptFrameworkService service) {
		this.service = service;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return service.getFrameworks();
	}

	@GetMapping("/frameworks/{id}")
	public ResponseEntity<JavaScriptFramework> findFrameworkById(@PathVariable Long id) {
		return ResponseEntity.of(service.getFrameworkById(id));
	}

	@GetMapping(value = "/frameworks/search")
	public List<JavaScriptFramework> searchFrameworks(@RequestAttribute String nameInfix) {
		return service.getFrameworkByNameInfix(nameInfix);
	}

	@PostMapping("/frameworks")
	public ResponseEntity<Object> createFramework(@Valid @RequestBody JavascriptFrameworkForm form) {
		Optional<JavaScriptFramework> existingFramework = service.getFrameworkByName(form.getName());

		if (existingFramework.isEmpty()) {
			JavaScriptFramework newFramework = createFrameworkEntity(form);

			service.saveFramework(newFramework);
		}

		return ResponseEntity.ok().build();
	}

	@PostMapping("/frameworks/{frameworkId}/versions")
	public ResponseEntity<Object> createVersion(@RequestBody JavascriptFrameworkVersionForm form,
												@PathVariable Long frameworkId) {
		Optional<JavaScriptFramework> frameworkOpt = service.getFrameworkById(frameworkId);

		if (frameworkOpt.isPresent()) {
			JavaScriptFramework framework = frameworkOpt.get();
			boolean versionExists = framework.getVersions().stream()
					.map(JavaScriptFrameworkVersion::getName)
					.anyMatch(s -> s.equals(form.getName()));

			if (!versionExists) {
				service.saveVersion(createFrameworkVersionEntity(form, framework));
			}

			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("/frameworks/{id}")
	public ResponseEntity<Object> updateFramework(@PathVariable Long id, @Valid @RequestBody JavascriptFrameworkForm form) {
		Optional<JavaScriptFramework> frameworkOpt = service.getFrameworkById(id);

		if (frameworkOpt.isPresent()) {
			JavaScriptFramework framework = frameworkOpt.get();
			framework.setName(form.getName());
			framework.setHypeLevel(form.getHypeLevel());

			service.saveFramework(framework);

			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/frameworks/{id}")
	public ResponseEntity<Object> deleteFramework(@PathVariable Long id) {
		Optional<JavaScriptFramework> existingFramework = service.getFrameworkById(id);

		if (existingFramework.isPresent()) {
			service.deleteFramework(existingFramework.get());

			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	private JavaScriptFramework createFrameworkEntity(JavascriptFrameworkForm form) {
		return JavaScriptFramework.builder()
				.name(form.getName())
				.hypeLevel(form.getHypeLevel())
				.build();
	}

	private JavaScriptFrameworkVersion createFrameworkVersionEntity(JavascriptFrameworkVersionForm form, JavaScriptFramework framework) {
		return JavaScriptFrameworkVersion.builder()
				.name(form.getName())
				.framework(framework)
				.deprecationDate(form.getDeprecationDate())
				.build();
	}

}
