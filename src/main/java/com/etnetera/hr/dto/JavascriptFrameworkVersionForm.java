package com.etnetera.hr.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class for mapping form parameters to javascript framework version
 */
@Getter
@Setter
@Builder
public class JavascriptFrameworkVersionForm {

	@NotNull
	private String name;

	@NotNull
	private LocalDateTime deprecationDate;
}
