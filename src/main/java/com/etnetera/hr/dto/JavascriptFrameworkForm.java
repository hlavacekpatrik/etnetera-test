package com.etnetera.hr.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Class for mapping form parameters to javascript framework
 */
@Getter
@Setter
public class JavascriptFrameworkForm {

	@NotNull
	private String name;

	@NotNull
	private Integer hypeLevel;
}
